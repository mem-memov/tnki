<?php

namespace App\Currency;

class States
{
    private $withCode;
    
    public function __construct() {
        $this->withCode = [];
    }
    
    public function hasStateWithCode(string $code): bool
    {
        return array_key_exists($code, $this->withCode);
    }
    
    public function getStateWithCode(string $code): State
    {
        if ( ! array_key_exists($code, $this->withCode) ) {
            throw new \Exception();
        }
        
        return $this->withCode[$code];
    }
    
    public function addState(State $state): void
    {
        $this->withCode[$state->getCode()] = $state;
    }
}
