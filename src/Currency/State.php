<?php

namespace App\Currency;

class State
{
    private $id;
    private $code;
    private $rate;
    private $previous;
    
    public function __construct(
        int $id = null,
        string $code = null,
        float $rate = null,
        State $previous = null
    ) {
        $this->id = $id;
        $this->code = $code;
        $this->rate = $rate;
        $this->previous = $previous;
    }

    public function getId(): int
    {
        return $this->id;
    }
    
    public function setId(int $id): self
    {
        return new self(
            $id,
            $this->code,
            $this->rate,
            $this
        );
    }
    
    public function getCode(): string
    {
        return $this->code;
    }
    
    public function setCode(string $code): self
    {
        return new self(
            $this->id,
            $code,
            $this->rate,
            $this
        );
    }
    
    public function getRate(): float
    {
        return $this->rate;
    }
    
    public function setRate(float $rate): self
    {
        return new self(
            $this->id,
            $this->code,
            $rate,
            $this
        );
    }
}
