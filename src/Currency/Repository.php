<?php

namespace App\Currency;

class Repository
{
    private $factory;
    private $storage;
    private $states;
    
    public function __construct(
        Factory $factory,
        Storage $storage,
        States $states
    ) {
        $this->factory = $factory;
        $this->storage = $storage;
        $this->states = $states;
    }
    
    public function findByCode(string $code): Entity
    {
        if ( $this->states->hasStateWithCode($code) ) {
            return $this->states->getStateWithCode($code);
        }
        
        $state = $this->storage->findByCode($code);
        
        $this->states->addState($state);
        
        return $this->factory->make($state);
    }
}
