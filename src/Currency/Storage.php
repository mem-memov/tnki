<?php

namespace App\Currency;

interface Storage
{
    public function findByCode(string $code): State;
}
