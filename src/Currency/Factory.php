<?php

namespace App\Currency;

class Factory
{
    public function make(State $state): Entity
    {
        return new Entity(
            $state
        );
    }
}
