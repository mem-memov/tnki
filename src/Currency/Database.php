<?php

namespace App\Currency;

class Database implements Storage
{
    private $connection;
    private $http;
    
    public function __construct(
        $connection,
        Http $http
    ) {
        $this->connection = $connection;
        $this->http = $http;
    }
    
    public function findByCode(string $code): State
    {
        $row = $this->connection->getAssoc(
            '
                SELECT
                    `id`,
                    `code`,
                    `rate`
                FROM
                    `currency`
                ;
            '
        );
        
        if ( !empty($row) ) {
            return new State(
                $row['id'],
                $row['code'],
                $row['rate']
            );
        }
        
        $state = $this->http->findByCode($code);
        
        $this->connection->query(
            '
                INSERT INTO
                    `currency`
                SET
                    `id` = :id,
                    `code` = :code,
                    `rate` = :rate
                ;
            ',
            [
                ':id' => $row['id'],
                ':code' => $row['code'],
                ':rate' => $row['rate']
            ]
        );
        
        return $state;
    }
}
