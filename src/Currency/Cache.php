<?php

namespace App\Currency;

use Psr\SimpleCache\CacheInterface;

class Cache implements Storage
{
    private $connection;
    private $database;
    
    public function __construct(
        CacheInterface $connection,
        Database $database
    ) {
        $this->connection = $connection;
        $this->database = $database;
    }
    
    public function findByCode(string $code): State
    {
        $key = 'currency.' . $code;
        
        if ( $this->connection->has($key) ) {
            $data = $this->connection->get($key);
            
            return new State(
                $data['id'],
                $data['code'],
                $data['rate']
            );
        }
        
        $state = $this->database->findByCode($key);
        
        $data = $this->connection->set([
            'id' => $state->getId(),
            'code' => $state->getCode(),
            'rate' => $state->getRate()
        ]);
        
        return $state;
    }
}
