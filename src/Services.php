<?php

namespace App;

use Psr\SimpleCache\CacheInterface;

class Service
{
    private $database;
    private $cache;
    private $instances;
    
    public function __construct(
        $database,
        CacheInterface $cache
    ) {
        $this->database = $database;
        $this->cache = $cache;
        $this->instances = [];
    }
    
    public function getCurrencies()
    {
        $key = 'currencies';
        
        if ( ! isset($this->instances[$key]) ) {
            $this->instances[$key] = new \App\Currency\Repository(
                new \App\Currency\Factory(),
                new \App\Currency\Cache(
                    $this->cache,
                    new \App\Currency\Database(
                        $this->database,
                        new \App\Currency\Http()
                    )
                )
            );
        }
        
        return $this->instances[$key];
    }
}
